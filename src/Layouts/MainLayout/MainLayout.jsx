import { Outlet } from 'react-router-dom';
import { Sidebar } from 'components/Sidebar';
import { Box } from '@chakra-ui/react';
import cls from './styles.module.scss';
export const MainLayout = () => {
  return (
    <>
      <Box className={cls.content}>
        <Sidebar />
        <Box className={cls.wrapper}>
          <Outlet className={cls.outlet} />
        </Box>
      </Box>
    </>
  );
};
