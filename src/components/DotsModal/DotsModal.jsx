import { Box, Button, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/react";
import { useState } from "react";
import cls from "styles.module.scss";
export const DotsModal = ({
    // isOpen, 
    // onClose = () => {},
    // Title="Добавить",
    // cancelText = "Отменить",
    // text = "Отправить",
    // handleAccept = () => {}, 
    // size,
    // children
    openModal = () => {},
    closeModal = () => {},
    }) => {

    const [modalIsOpen, setModalIsOpen] = useState(false);
    const openModal = () => {
        setModalIsOpen(true);
    };

    const closeModal = () => {
        setModalIsOpen(false);
    };

    return (
        <div>
            <button onClick={openModal}>Открыть модальное окно</button>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="Example Modal"
            >
                <h2>Модальное окно</h2>
                <p>Текст модального окна...</p>
                <button onClick={closeModal}>Закрыть</button>
            </Modal>
        </div>
    );
}