import { Box } from "@chakra-ui/react";
import cls from "./styles.module.scss";

export const BtnSubmit = (props) => {

    return (
        <button className={cls.btn} onClick={() => props.onClick()} disabled={props.disabled}>
            <span className={cls.plus}>+</span>
            {props.text}
        </button>
    )
}