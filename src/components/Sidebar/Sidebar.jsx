import { Link } from 'react-router-dom';
import cls from './styles.module.scss';
import { Box, Button } from '@chakra-ui/react';
import { Footer } from 'components/Footer';
import Courses from 'assets/img/icon/courses.svg';
import UserIcon from 'assets/img/icon/usersicon.svg';
import InfoIcon from 'assets/img/icon/info.svg';
import Reports from 'assets/img/icon/reports.svg';
import GroupsIcon from 'assets/img/icon/groupsIcon.svg';
import DashbordIcon from 'assets/img/icon/DashboardIcon.svg';
import Payment from 'assets/img/icon/Payment.svg';
import Logout from 'assets/img/icon/logout.svg';
import { authStore } from 'store/auth.store';
import { AiOutlineAlignLeft } from 'react-icons/ai';
import { AiOutlineAlignRight } from 'react-icons/ai';
import { useState } from 'react';
export const Sidebar = ({ openNav, closeNav }) => {
  const links = [
    {
      title: 'Дашборд',
      path: 'admin/dashbord',
      src: DashbordIcon,
      alt: 'dashbordIcon',
    },
    {
      title: 'Курсы',
      path: 'admin/courses',
      src: Courses,
      alt: 'Courses',
    },
    {
      title: 'Группы',
      path: 'admin/groups',
      src: GroupsIcon,
      alt: 'Группы',
    },
    {
      title: 'Ученики',
      path: 'admin/students',
      src: UserIcon,
      alt: 'UserIcon',
    },
    {
      title: 'Менторы',
      path: 'admin/mentors',
      src: UserIcon,
      alt: 'UserIcon',
    },
    {
      title: 'Платежи',
      path: 'admin/payments',
      src: Payment,
      alt: 'payment',
    },
    {
      title: 'Отчеты',
      path: 'admin/reports',
      src: Reports,
      alt: 'отчеты',
    },
    {
      title: 'Профиль',
      path: 'admin/profile',
      src: InfoIcon,
      alt: 'InfoIcon',
    },
  ];
  const auth = JSON.parse(localStorage.getItem('auth'));
  const [openItem, setOpenItem] = useState(links.title);
  console.log(openItem);
  function openNav() {
    document.getElementById('mySidenav').style.width = '280px';
    document.getElementById('mySidenav').style.width = '280px';
  }

  function closeNav() {
    document.getElementById('mySidenav').style.width = '88px';
  }
  const handleLogOut = () => {
    localStorage.clear();
    authStore.logout();
  };
  const acttive = () => {
    links.title + ' ' + 'active';
  };
  // const [active, setActive] = useState(null)
  return (
    <Box className={cls.sidebar} id="mySidenav">
      <Box className={cls.wrapper}>
        <Link to={'/admin/dashbord'} className={cls.logo}>
          LMS
        </Link>

        <button type="button">
          {links.title === openItem ? (
            <button className={cls.sidebarBtn} onClick={() => setOpenItem('')}>
              <Box className={cls.closeBtn} onClick={closeNav}>
                <AiOutlineAlignRight />
              </Box>
            </button>
          ) : (
            <button className={cls.sidebarBtn} onClick={() => setOpenItem(links.title)}>
              <Box className={cls.closeBtn} onClick={openNav}>
                <AiOutlineAlignLeft />
              </Box>
            </button>
          )}
        </button>
      </Box>
      <nav className={cls.navbar}>
        <ul className={cls.navList} id="Nav">
          {links?.map((link, index) => (
            <li onClick={acttive} className={cls.navItem} key={index}>
              <Link className={cls.navLink} to={link?.path}>
                <img src={link?.src} alt={link?.alt} width={20} height={20} />
                {links.title == openItem && <span className={cls.navText}>{link?.title}</span>}
              </Link>
            </li>
          ))}
          <li className={cls.navItem}>
            <button className={cls.navLink} onClick={handleLogOut}>
              <img src={Logout} alt="logout" width={20} height={20} />
              {links.title == openItem && <span className={cls.navText}>Выход</span>}
            </button>
          </li>
        </ul>
      </nav>
      {links.title === openItem ? <Footer auth={auth} /> : ''}
    </Box>
  );
};
