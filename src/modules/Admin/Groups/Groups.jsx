import { Box, Input, Select, TabPanels } from '@chakra-ui/react';
import { Container } from 'components/Container';
import { Header } from 'components/Header';
import { useGroupsProps } from './useGroupsProps';
import { CustomTable } from 'components/CustomTable';
import cls from './styles.module.scss';
import { Pagination } from 'components/Pagination';
import { EditModal } from 'components/EditModal';
import { Button, FormControl } from 'react-bootstrap';
import { Tab, TabList, TabPanel, Tabs } from '@chakra-ui/react';
import { AddModal } from 'components/AddModal';
export const Groups = () => {
  const {
    data,
    currentRecords,
    userMentors,
    isOpen,
    onClose,
    onOpen,
    columns,
    onSubmit,
    nPages,
    handleDeleteUser,
    activeGroupId,
    handleEdit,
    currentPage,
    setCurrentPage,
    register,
    handleSubmit,
    onChange,
    setActiveGroupId,
  } = useGroupsProps();

  return (
    <Box mt={100}>
      <Container>
        <Header
          title="Группы"
          onOpen={onOpen}
          isOpen={isOpen}
          onClose={onClose}
          onChange={onChange}
          handleAccept={handleSubmit(onSubmit)}
          register={register}
        />
        <Box>
          <Tabs background="#fff">
            <TabList>
              {data?.map((tab, index) => (
                <Tab key={index}>{tab.name}</Tab>
              ))}
            </TabList>
            <TabPanels>
              {data?.map((tab, index) => (
                <TabPanel p={4} key={index}>
                  <CustomTable columns={columns} data={data} />
                </TabPanel>
              ))}
            </TabPanels>
          </Tabs>
        </Box>

        <AddModal handleAccept={handleSubmit(onSubmit)} isOpen={isOpen} register={register} onClose={onClose}>
          <FormControl as="form">
            <Box classNhandleEditame={cls.wrapper}>
              <Select {...register('course_id')} placeholder="Название потока">
                {currentRecords?.map((item, index) => {
                  return (
                    <option key={index} value={item?.id}>
                      {item?.name}
                    </option>
                  );
                })}
              </Select>
            </Box>
            <Box className={cls.wrapper}>
              <input type="text" placeholder="group" {...register('name')} />
            </Box>
            <Box className={cls.wrapper}>
              <Select {...register('type')}>
                <option value={'Online'}>Online </option>
                <option value={'Offline'}>Offline </option>
              </Select>
            </Box>
            <Box className={cls.wrapper}>
              <Select placeholder="Учитель" {...register('teacher_id')}>
                {userMentors?.map((item, index) => (
                  <option key={index} value={item?.id}>
                    {item?.first_name}
                  </option>
                ))}
              </Select>
            </Box>
            <Box className={cls.wrapper}>
              <Box className={cls.wrapper}>
                <label htmlFor="end data">start data</label>

                <Input {...register('start_date')} type="date" placeholder="Дата начала" />
              </Box>

              <Box className={cls.wrapper}>
                <label htmlFor="end data">end data</label>
                <Input {...register('end_date')} type="date" placeholder="Дата начала" />
              </Box>
            </Box>
          </FormControl>
        </AddModal>

        <Pagination nPages={nPages} currentPage={currentPage} setCurrentPage={setCurrentPage} />
      </Container>
    </Box>
  );
};
{
  /* <EditModal
          handleDeleteGroup={handleDeleteUser}
          handleAccept={handleSubmit(handleEdit)}
          id={activeGroupId}
          isOpen={isOpen}
          register={register}
          onClose={() => {
            onClose();
            setActiveGroupId('');
          }}
        >
          <FormControl as="form">
            <Box className={cls.wrapper}>
              <option value="option1">Active </option>
              <option value="option1">Pasive </option>
            </Box>
          </FormControl>
        </EditModal> */
}
