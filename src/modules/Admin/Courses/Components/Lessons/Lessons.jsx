// import { Box, Button, FormControl, useDisclosure } from "@chakra-ui/react";
// import cls from "./styles.module.scss";
// // import { Container, Draggable } from 'react-smooth-dnd';
// import { useEffect, useRef, useState } from "react";
// import DragnDrop from "assets/img/icon/dragndrop.svg";
// import { AddModal } from "components/AddModal";
// import { useCreateLesson } from "services/lessons.service";
// import { useForm } from "react-hook-form";
// import { Input } from "components/Input";
// import { useNavigate } from "react-router-dom";
// import { BtnSubmit } from "components/BtnSubmit";
// import { EditIcon } from '@chakra-ui/icons'
// import { useMutation } from "@tanstack/react-query";

// export const Lessons = ({
//   activeCourse 
// }) => {
//   const navigate = useNavigate()
//   const { isOpen, onOpen, onClose } = useDisclosure();
//   console.log(activeCourse.id)
//   const [items, setItems] = useState([]);
//   const [image, setImage] = useState()
//   // console.log(items)

//   useEffect(() => {
//     setItems(activeCourse?.lesson_names)
//   }, [items])

//   const { 
//     register, 
//     handleSubmit, 
//     reset 
//   } = useForm();

//   // const { mutate: createLesson } = useCreateLesson()

//   // const { mutate: createLesson } = useMutation({ mutationFn: (data) => fetch('https://lms-service-xhvq.onrender.com/api/v1/upload', { method: 'POST', body: JSON.stringify(data) })})
    
//   // const onSubmit = (data) => {
//   //   createLesson({
//   //   // course_id: activeCourse?.id,
//   //   ...image
//   //   }, {
//   //     onSuccess: () => {
//   //       // refetch();
//   //       onClose()
//   //     }
//   //   })
//   // }

//   const fileInputRef = useRef(null);

//   const onSubmit = async (e) => {
//     e.preventDefault();

//     const file = fileInputRef.current.files[0];
//     const formData = new FormData();
//     formData.append('file', file);

//     try {
//       const response = await fetch('https://lms-service-xhvq.onrender.com/api/v1/upload', {
//         method: 'POST',
//         body: formData
//       });
//       if (response.ok) {
//         alert('Video uploaded successfully');
//         onClose()
//       } else {
//         alert('Failed to upload video');
//       }
//     } catch (error) {
//       alert('Error uploading video:', error);
//     }
//   }

//   const onDrop = (dropResult) => {
//     if (!dropResult.removedIndex) return;
//     const newItems = [...items];
//     newItems.splice(dropResult.addedIndex, 0, newItems.splice(dropResult.removedIndex, 1)[0]);
//     setItems(newItems);
//   };

//   const handleClick = () => {
//     fileInputRef.current.click(); 
//   };
//   return (
//     <Box className={cls.wrapper}>
//       {/* <Container onDrop={onDrop}>
//         <Box className={cls.wrapperTop}>
//           <h2 className={cls.title}>Course chapters</h2>
//           <BtnAdd onClick={osnOpen} />
//         </Box>
//         {
//         items?.map((item) => (
//           <Draggable key={item.id}>
//             <Box className={cls.wrapperBottom}>
//               <Box className={cls.wrapperTitle}>
//                 <img src={DragnDrop} alt="dragndrop" width="10px" height="10px"/>
//                 <h3 className={cls.subtitle}>{item.name}</h3>
//               </Box>
//               <Button
//                 backgroundColor="transparent"   
//                 onClick={() => 
//                   navigate(`/admin/courses/${activeCourse?.id}/${item.id}`)
//                 }
//               >
//                 <EditIcon />
//               </Button>
//             </Box>
//           </Draggable>
//           ))
//         }
//       </Container>
//       <AddModal handleAccept={onSubmit} register={register} isOpen={isOpen} onClose={onClose} >
//           <FormControl as='form'>
//             <Box className={cls.wrapperInput}>
//               <label htmlFor="title">title</label>
//               <Input
//                 type="text"
//                 placeholder="title"
//                 id="title"
//                 // register={register}
//                 name="name"
//               />
//             </Box>
//             <Box className={cls.wrapperInput}>
//               <label htmlFor="description">description</label>
//               <textarea className={cls.desc} name="description" id="description" cols="10" rows="3"></textarea>
//             </Box>
//             <Box className={cls.wrapperInput}>
//               <button className={cls.btn} onClick={handleClick} type="button">Выбрать видео</button>
//               <video width={400} height={400} src={'https://firebasestorage.googleapis.com/v0/b/lms-service-415606.appspot.com/o/video.mp4?alt=media&token=8466d439-0058-4f10-bc21-2dd19b83fa06'} controls></video>
//               <input
//                 className={cls.fileInput}
//                 type="file"
//                 ref={fileInputRef}
//                 register={register}
//                 name="file"
//               />
//             </Box>
//           </FormControl>
//         </AddModal> */}
//     </Box>
//   );
// }

// import { useEffect, useState } from "react";
// import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
// export const Lessons = () => {
//   const [items, setItems] = useState([
//     { id: '1', name: 'Item 1' },
//     { id: '2', name: 'Item 2' },
//     { id: '3', name: 'Item 3' },
//   ]);

//   const onDragEnd = (result) => {
//     if (!result.destination) {
//       return;
//     }

//     const newItems = Array.from(items);
//     const [removed] = newItems.splice(result.source.index, 1);
//     newItems.splice(result.destination.index, 0, removed);

//     setItems(newItems);
//   };

//   return (
//     <DragDropContext onDragEnd={onDragEnd}>
//       <Droppable droppableId="droppable">
//         {(provided) => (
//           <div {...provided.droppableProps} ref={provided.innerRef}>
//             {items.map((item, index) => (
//               <Draggable key={item.id} draggableId={item.id} index={index}>
//                 {(provided) => (
//                   <div
//                     ref={provided.innerRef}
//                     {...provided.draggableProps}
//                     {...provided.dragHandleProps}
//                   >
//                     {item.name}
//                   </div>
//                 )}
//               </Draggable>
//             ))}
//             {provided.placeholder}
//           </div>
//         )}
//       </Droppable>
//     </DragDropContext>
//   )
// }






// import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
// import cls from "./styles.module.scss";
// import { AddModal } from "components/AddModal";
// import { Box, FormControl } from "@chakra-ui/react";
// import { Input } from "components/Input";

// export const Lessons = ({
//   isOpen,
//   handleOpen = () => {},
//   handleClose = () => {},
//   handleClick = () => {},
//   handleDragAndDrop = () => {},
//   handleCreateLesson = () => {},
//   register = () => {},
//   stores = [],
//   fileInputRef = () => {},
// }) => {

//   // console.log(stores)

//   return (
//     <div className={cls.layoutWrapper}>
//       <div className={cls.card}>
//         <DragDropContext onDragEnd={handleDragAndDrop}>
//           <div className={cls.header}>
//             <h1>Shopping List</h1>
//           </div>
//           <Droppable droppableId="ROOT" type="group">
//             {(provided) => (
//               <div {...provided.droppableProps} ref={provided.innerRef}>
//                 {stores?.map((store, index) => (
//                   <Draggable
//                     draggableId={store.id}
//                     index={index}
//                     key={store.id}
//                   >
//                     {(provided) => (
//                       <div
//                         {...provided.dragHandleProps}
//                         {...provided.draggableProps}
//                         ref={provided.innerRef}
//                       >
//                         <StoreList {...store} />
//                       </div>
//                     )}
//                   </Draggable>
//                 ))}
//                 {provided.placeholder}
//               </div>
//             )}
//           </Droppable>
//         </DragDropContext>
//       </div>
//       <AddModal handleAccept={handleCreateLesson} register={register} isOpen={isOpen} onClose={handleClose} >
//         <FormControl as='form'>
//           <Box className={cls.wrapperInput}>
//             <label htmlFor="title">title</label>
//             <Input
//              type="text"
//              placeholder="title"
//              id="title"
//              register={register}
//              name="title"
//            />
//          </Box>
//          <Box className={cls.wrapperInput}>
//            <label htmlFor="description">description</label>
//            <textarea className={cls.desc} name="description" id="description" cols="10" rows="3"></textarea>
//          </Box>
//          <Box className={cls.wrapperInput}>
//            <button className={cls.btn} onClick={handleClick} type="button">Выбрать видео</button>
//            <video width={400} height={400} src={'https://firebasestorage.googleapis.com/v0/b/lms-service-415606.appspot.com/o/video.mp4?alt=media&token=8466d439-0058-4f10-bc21-2dd19b83fa06'} controls></video>
//            <input
//              className={cls.fileInput}
//              type="file"
//              ref={fileInputRef}
//              register={register}
//              name="file"
//            />
//           </Box>
//         </FormControl>
//       </AddModal> 
//     </div>
//   );
// }

import cls from "./styles.module.scss"
import { Container, Draggable } from 'react-smooth-dnd';
import { AddModal } from "components/AddModal";
import { Box, Button, FormControl } from "@chakra-ui/react";
import { Input } from "components/Input";
import { BtnSubmit } from "components/BtnSubmit";
import DragnDrop from "assets/img/icon/dragndrop.svg";
import { EditIcon } from "@chakra-ui/icons";
import { BsThreeDots } from "react-icons/bs";
import { useState } from "react";
import { ChevronDownIcon } from '@chakra-ui/icons'

export const Lessons = ({
  navigate = () => {},
  handleCreateModule = () => {},
  modules = [],
  isOpen,
  handleOpen = () => {},
  handleClose = () => {},
  handleClick = () => {},
  handleDragAndDrop = () => {},
  handleCreateLesson = () => {},
  register = () => {},
  stores = [],
  lessons = [],
  fileInputRef = () => {},
}) => {

  console.log(modules?.modules)
  console.log(lessons)
  const [openItem, setOpenItem] = useState('')
  const [isLessonOpen, setLessonOpen] = useState(false);
  const handleLessonOpen = () => setLessonOpen(true);
  const handleLessonClose = () => {
    setLessonOpen(false);
    // reset();
  };

  return (
    <Box className={cls.content}>
      <Box className={cls.wrapperTop}>
        <h3 className={cls.title}>Создать модуль</h3>
        <BtnSubmit 
          text="Создать модуль"
          onClick={handleOpen}
        />
      </Box>
      <Container onDrop={handleDragAndDrop}>
      {
        modules?.modules?.map((item, index) => (
        <Draggable key={item?.id}>
          <button   
            key={index}
            type="button"
            id="Btn"
            onClick={() => (openItem ? setOpenItem('') : setOpenItem(course.id))} 
            className={cls.wrapperLessons}
          >
            <Box className={cls.wrapperTitle}>
              <img src={DragnDrop} alt="dragndrop" width="10px" height="10px"/>
              <h3 className={cls.subtitle}>{item?.name}</h3>
            </Box>
            <Box className={cls.wrapperBtn}>
              <BtnSubmit 
                text="Добавить урок"
                onClick={handleLessonOpen} 
              />
              <ChevronDownIcon
                ml={2} 
                width="28px"
                height="28px"
                color="blue"
              />
            </Box>
            {/* <button className={cls.wrapperBtn}> */}
              {/* <BsThreeDots /> */}

              {/* <img src={Dots} alt="dots" /> */}
              {/* <img src="" alt="" /> */}
              {/* <Button
                className={cls.wrapperBtn}
                backgroundColor="transparent"   
                onClick={() => 
                  navigate(`/admin/courses/${activeCourse?.id}/${item.id}`)
                }
              >
                <EditIcon />
              </Button>  */}
            {/* </button> */}
          </button>
        </Draggable>
        ))
      }
    </Container>
    <Container onDrop={handleDragAndDrop}>
      {
        lessons?.lessons?.map((item) => (
          <Draggable key={item?.id}>
            <Box className={cls.wrapperBottom}>
              <Box className={cls.wrapperTitle}>
                <img src={DragnDrop} alt="dragndrop" width="10px" height="10px"/>
                <h3 className={cls.subtitle}>{item?.name}</h3>
              </Box>
              <Button
                backgroundColor="transparent"   
                onClick={() => 
                  navigate(`/admin/courses/${activeCourse?.id}/${item.id}`)
                }
              >
                <EditIcon />
              </Button>
            </Box>
          </Draggable>
        ))
      }
    </Container>
      <AddModal handleAccept={handleCreateModule} register={register} isOpen={isOpen} onClose={handleClose} >
        <FormControl as='form'>
          <Box className={cls.wrapperInput}>
            <Input
             type="text"
             placeholder="Введите название модуля"
             id="name"
             register={register}
             name="name"
           />
         </Box>
        </FormControl>
      </AddModal> 
      <AddModal handleAccept={handleCreateLesson} register={register} isOpen={isLessonOpen} onClose={handleLessonClose} >
        <FormControl as='form'>
          <Box className={cls.wrapperInput}>
            <label htmlFor="title">title</label>
            <Input
            type="text"
            placeholder="title"
            id="title"
            register={register}
            name="title"
          />
        </Box>
        <Box className={cls.wrapperInput}>
          <label htmlFor="description">description</label>
          <textarea className={cls.desc} name="description" id="description" cols="10" rows="3"></textarea>
        </Box>
        <Box className={cls.wrapperInput}>
          <button className={cls.btn} onClick={handleClick} type="button">Выбрать видео</button>
            {/* <video width={400} height={400} src={'https://firebasestorage.googleapis.com/v0/b/lms-service-415606.appspot.com/o/video.mp4?alt=media&token=8466d439-0058-4f10-bc21-2dd19b83fa06'} controls></video>  */}
            <input
              className={cls.fileInput}
              type="file"
              ref={fileInputRef}
              register={register}
              name="file"
            />
          </Box>
        </FormControl>
      </AddModal>  
    </Box>
  )
}
  
// <AddModal handleAccept={handleCreateLesson} register={register} isOpen={isOpen} onClose={handleClose} >
//     <FormControl as='form'>
//       <Box className={cls.wrapperInput}>
//         <label htmlFor="title">title</label>
//         <Input
//          type="text"
//          placeholder="title"
//          id="title"
//          register={register}
//          name="title"
//        />
//      </Box>
//      <Box className={cls.wrapperInput}>
//        <label htmlFor="description">description</label>
//        <textarea className={cls.desc} name="description" id="description" cols="10" rows="3"></textarea>
//      </Box>
//      <Box className={cls.wrapperInput}>
//        <button className={cls.btn} onClick={handleClick} type="button">Выбрать видео</button>
//          <video width={400} height={400} src={'https://firebasestorage.googleapis.com/v0/b/lms-service-415606.appspot.com/o/video.mp4?alt=media&token=8466d439-0058-4f10-bc21-2dd19b83fa06'} controls></video> 
//         <input
//           className={cls.fileInput}
//           type="file"
//           ref={fileInputRef}
//           register={register}
//           name="file"
//         />
//       </Box>
//     </FormControl>
//   </AddModal>  