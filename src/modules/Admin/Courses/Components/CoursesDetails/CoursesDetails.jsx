import { Box, Tab, TabIndicator, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";
import { Container } from "components/Container";
import { useCoursesDetailsProps } from "./useCoursesDetailsProps";
import { Detail } from "../Detail";
import { Header } from "../Header";
import { Lessons } from "../Lessons";

export const CoursesDetails = () => {

  const {
    lessons, 
    navigate,
    handleCreateModule,
    modules,
    isOpen,
    handleOpen,
    handleClose,
    fileInputRef,
    handleClick,
    handleCreateLesson,
    handleDragAndDrop,
    stores,
    register,
    handleSubmit,
    handleDeleteCourse,
    handleUpdateCourse,
    activeCourse,
  } = useCoursesDetailsProps();

  return(
    <Box>
      <Header title={activeCourse?.name} />
        <Box background="#fff" borderRadius="8px" padding="0 10px" margin="68px  20px">
          <Tabs>
            <TabList>
              <Tab>Детали</Tab>
              <Tab>Уроки</Tab>
            </TabList>
            <TabPanels>
              <TabPanel>
                <Detail 
                  fileInputRef={fileInputRef}
                  handleClick={handleClick}
                  handleDeleteCourse={handleDeleteCourse}
                  handleUpdate={handleSubmit(handleUpdateCourse)}
                  register={register}
                  handleSubmit={handleSubmit}
                />
              </TabPanel>
              <TabPanel>
                <Lessons
                  lessons={lessons}
                  navigate={navigate}
                  handleCreateModule={handleCreateModule}
                  modules={modules}
                  fileInputRef={fileInputRef}
                  isOpen={isOpen}
                  handleOpen={handleOpen}
                  handleClose={handleClose}
                  handleCreateLesson={handleCreateLesson}
                  register={register}
                  handleClick={handleClick}
                  handleDragAndDrop={handleDragAndDrop}
                  stores={stores} 
                />
              </TabPanel>
            </TabPanels>
          </Tabs>
        </Box>
    </Box>
  ) 
}
