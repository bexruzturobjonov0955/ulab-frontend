import { useForm } from 'react-hook-form';
import { useEffect, useRef, useState } from 'react';
import { useDeleteCourseById, useGetCourseById, useGetCourses, useUpdateCourseById } from 'services/courses.service';
import { useNavigate, useParams } from 'react-router-dom';
import request from 'services/httpRequest';
import { useToast } from '@chakra-ui/react';
import { useCreateModule, useDeleteModuleById, useGetModuleById, useGetModules } from 'services/module.service';
import { useGetLessons } from 'services/lessons.service';

export const useCoursesDetailsProps = () => {
  const [isOpen, setOpen] = useState(false);
  const [activeCourse, setActiveCourse] = useState('');
  const [stores, setStores ] = useState([])
  const { register, handleSubmit, reset } = useForm();
  const fileInputRef = useRef(null);
  const toast = useToast();
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    request.get(`course/${id}`).then((res) => setActiveCourse(res?.data));
  }, [id]);

  useEffect(() => {
    setStores(activeCourse?.lesson_names);  
  }, [activeCourse?.lesson_names]);

  const { refetch } = useGetCourses();
  const { data: getCourseById, isSuccess } = useGetCourseById({ courseId: activeCourse?.id });
  const { mutate: updateCourse } = useUpdateCourseById();
  const { mutate: deleteCourse } = useDeleteCourseById();

  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    reset();
  };

  useEffect(() => {
    if (isSuccess && activeCourse?.id) {
      reset({
        photo: getCourseById?.photo,
        name: getCourseById?.name,
        for_who: getCourseById?.for_who,
        type: getCourseById?.type,
        weekly_number: getCourseById?.weekly_number,
        duration: getCourseById?.duration,
        price: getCourseById?.price,
      });
    }
  }, [getCourseById]);

  const handleUpdateCourse = (res) => {
    const data = {
      photo: res?.photo,
      name: res?.name,
      type: res?.type,
      beginning_date_course: res?.beginning_date_course,
      for_who: res?.for_who,
      weekly_number: res?.weekly_number - 0,
      duration: res?.duration,
      price: res?.price - 0,
    };

    updateCourse({ id: activeCourse?.id, ...data }, {
      onSuccess: () => {
        navigate("/admin/courses");
        refetch();
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: "Вы успешно изменили данные курса",
          status: 'success',
        });
      },
      onError: (error) => {
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: error?.response?.data,
          status: 'error',
        });
      }
    });
  };

  const handleDeleteCourse = (data) => {
    deleteCourse({ id: activeCourse?.id, ...data }, {
      onSuccess: () => {
        navigate("/admin/courses");
        refetch();
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: "Вы успешно удалили курс",
          status: 'success',
        });
      },
      onError: (error) => {
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: error?.response?.data,
          status: 'error',
        });
      }
    });
  };
  
  const handleDragAndDrop = (dropResult) => {
    if (!dropResult.removedIndex) return;
    const newItems = [...items];
    newItems.splice(dropResult.addedIndex, 0, newItems.splice(dropResult.removedIndex, 1)[0]);
    setItems(newItems);
  };

  const { mutate: createModule } = useCreateModule()
  const { mutate: deleteModule } = useDeleteModuleById()
  const { data: modules, refetch: refetchModules } = useGetModules({ courseId: activeCourse?.id })
  // const { data: modulesById} = useGetModuleById()
  const handleCreateModule = (data) => {
    createModule({ 
      ...data,  
      course_id: activeCourse?.id,
    }, {
      onSuccess: () => {
        // navigate("/admin/courses");
        refetch();
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: "Вы успешно удалили курс",
          status: 'success',
        });
      },
      onError: (error) => {
        toast({
          position: 'top center',
          duration: 3000,
          isClosable: true,
          title: error?.response?.data,
          status: 'error',
        });
      }
    });
  };


  const { data: lessons } = useGetLessons({ courseId: activeCourse?.id })

  const handleCreateLesson = async (e) => {
    e.preventDefault();

    const file = fileInputRef.current.files[0];
    const formData = new FormData();
    formData.append('file', file);

    try {
      const response = await fetch(`https://lms-back.nvrbckdown.uz/lms/api/v1/upload/`, {
        method: 'POST',
        body: formData
      });

      if (response.ok) {
        alert('Video uploaded successfully');
        onClose();
      } else {
        alert('Failed to upload video');
      }
    } catch (error) {
      alert('Error uploading video:', error);
    }
  };

  const handleClick = () => {
    fileInputRef.current.click();
  };

  return {
    navigate,
    modules,
    lessons,
    handleCreateModule,
    fileInputRef,
    handleClick: () => fileInputRef.current.click(),
    activeCourse,
    fileInputRef,
    handleCreateLesson,
    handleClick,
    stores,
    handleDragAndDrop,
    isOpen,
    handleOpen,
    handleClose,
    register,
    handleSubmit,
    handleDeleteCourse,
    handleUpdateCourse,
  };
};












// import { useForm } from 'react-hook-form';
// import { useEffect, useRef, useState } from 'react';
// import { useDeleteCourseById, useGetCourses, useGetCourseById, useCreateCourse, useUpdateCourseById } from 'services/courses.service';
// import { useNavigate, useParams } from 'react-router-dom';
// import request from 'services/httpRequest';
// import { useToast } from '@chakra-ui/react';
// export const useCoursesDetailsProps = () => {

//   const [isOpen, setOpen] = useState(false);
//   const [activeCourse, setActiveCourse] = useState("")
  
//   const activeCourseId = activeCourse?.id

//   const toast = useToast()

//   const handleOpen = () => setOpen(true);

//   const handleClose = () => {
//     setOpen(false);
//     reset();
//   };
  
//   const param = useParams();
//   const navigate = useNavigate();

//   const { 
//     register, 
//     handleSubmit, 
//     reset 
//   } = useForm();

//   useEffect(() => {
//     request.get(`course/${param.id}`)
//     .then(res => setActiveCourse(res.data))
//   }, [])

//   const { refetch } = useGetCourses();

//   const { data: getCourseById, isSuccess } = useGetCourseById({ courseId: activeCourseId });

//   const { mutate: updateCourse } = useUpdateCourseById();

//   const handleUpdateCourse = (res) => {
//     const data = {
//       photo: res?.photo,
//       name: res?.name,
//       type: res?.type,
//       beginning_date_course: res?.beginning_date_course,
//       for_who: res?.for_who,
//       weekly_number: res?.weekly_number - 0,
//       duration: res?.duration,
//       price: res?.price - 0,
//     }
//     updateCourse({
//       id: activeCourseId,
//       ...data
//     }, {
//         onSuccess: () => {
//           navigate("/admin/courses")
//           refetch();
//           toast({
//             position: 'top center',
//             duration: 3000,
//             isClosable: true,
//             title: "Вы успешно изменили данные курса",
//             status: 'success',
//           })
//         },
//         onError: (error) => {
//           toast({
//             position: 'top center',
//             duration: 3000,
//             isClosable: true,
//             title: error?.response?.data,
//             status: 'error',
//           }) 
//         }
//     });
//   };

//   const { mutate: deleteCourse } = useDeleteCourseById();

//   const handleDeleteCourse = (data) => {
//     deleteCourse({ 
//       id: activeCourseId,
//       ...data 
//     }, {
//         onSuccess: () => {
//           navigate("/admin/courses")
//           refetch();
//           toast({
//             position: 'top center',
//             duration: 3000,
//             isClosable: true,
//             title: "Вы успешно удалили курс",
//             status: 'success',
//           })
//         },
//         onError: (error) => {
//           toast({
//             position: 'top center',
//             duration: 3000,
//             isClosable: true,
//             title: error?.response?.data,
//             status: 'error',
//           })
//         }
//     });
//   };

//   useEffect(() => {
//     if (isSuccess && activeCourseId) {
//       reset({
//         photo: getCourseById?.photo,
//         name: getCourseById?.name,
//         for_who: getCourseById?.for_who,
//         type: getCourseById?.type,
//         weekly_number: getCourseById?.weekly_number,
//         duration: getCourseById?.duration,
//         price: getCourseById?.price,
//       });
//     }
//   }, [getCourseById]);

//   /* Lessons */

//   const [stores, setStores] = useState([]);
//   const fileInputRef = useRef(null);

//   useEffect(() => {
//     setStores(activeCourse?.lesson_names);  
//   }, [activeCourse?.lesson_names]);

//   // useEffect(() => {
//   //   console.log(stores);
//   // }, [stores]);

//   const handleDragAndDrop = (results) => {
//     // const { source, destination, type } = results;

//     // if (!destination) return;

//     // if (
//     //   source.droppableId === destination.droppableId &&
//     //   source.index === destination.index
//     // )
//     //   return;

//     // if (type === "group") {
//     //   const reorderedStores = [...stores];

//     //   const storeSourceIndex = source.index;
//     //   const storeDestinatonIndex = destination.index;

//     //   const [removedStore] = reorderedStores.splice(storeSourceIndex, 1);
//     //   reorderedStores.splice(storeDestinatonIndex, 0, removedStore);

//     //   return setStores(reorderedStores);
//     // }
//     // const itemSourceIndex = source.index;
//     // const itemDestinationIndex = destination.index;

//     // const storeSourceIndex = stores.findIndex(
//     //   (store) => store.id === source.droppableId
//     // );
//     // const storeDestinationIndex = stores.findIndex(
//     //   (store) => store.id === destination.droppableId
//     // );

//     // const newSourceItems = [...stores[storeSourceIndex].items];
//     // const newDestinationItems =
//     //   source.droppableId !== destination.droppableId
//     //     ? [...stores[storeDestinationIndex].items]
//     //     : newSourceItems;

//     // const [deletedItem] = newSourceItems.splice(itemSourceIndex, 1);
//     // newDestinationItems.splice(itemDestinationIndex, 0, deletedItem);

//     // const newStores = [...stores];

//     // newStores[storeSourceIndex] = {
//     //   ...stores[storeSourceIndex],
//     //   items: newSourceItems,
//     // };
//     // newStores[storeDestinationIndex] = {
//     //   ...stores[storeDestinationIndex],
//     //   items: newDestinationItems,
//     // };

//     // setStores(newStores);
//   };

//   const handleCreateLesson = async (e) => {
//     e.preventDefault();

//     const file = fileInputRef.current.files[0];
//     const formData = new FormData();
//     formData.append('file', file);

//     try {
//       const response = await fetch('https://lms-service-xhvq.onrender.com/api/v1/upload', {
//         method: 'POST',
//         body: formData
//       });
//       if (response.ok) {
//         alert('Video uploaded successfully');
//         onClose()
//       } else {
//         alert('Failed to upload video');
//       }
//     } catch (error) {
//       alert('Error uploading video:', error);
//     }
//   }

//   const handleClick = () => {
//     fileInputRef.current.click(); 
//   };
  
//   return {
//     activeCourse,
//     fileInputRef,
//     handleCreateLesson,
//     handleClick,
//     stores,
//     handleDragAndDrop,
//     isOpen,
//     handleOpen,
//     handleClose,
//     register,
//     handleSubmit,
//     handleDeleteCourse,
//     handleUpdateCourse,
//   };
// };
